FROM java:8-jre
WORKDIR usr/src
ADD ./target/demo-app-0.0.1-SNAPSHOT.jar /usr/src/app.jar
ENTRYPOINT ["java","-jar","app.jar"]