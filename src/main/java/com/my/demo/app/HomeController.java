package com.my.demo.app;

import java.time.LocalTime;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController{

    @GetMapping("/")
    public ModelAndView home(){
        ModelAndView mav = new ModelAndView("home");
        mav.addObject("message", "Hello, World! The time is : "+LocalTime.now());
        return mav;
    }
}